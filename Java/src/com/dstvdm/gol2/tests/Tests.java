package com.dstvdm.gol2.tests;


import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Joshua.Lewis on 2014/06/27.
 */
public class Tests {

    @Test
    public void Test_Barge() {
        char[][] initialGrid = new char[][]
                {
                        {'.', '.', '.', '.', '.', '.'},
                        {'.', '.', '*', '.', '.', '.'},
                        {'.', '*', '.', '*', '.', '.'},
                        {'.', '.', '*', '.', '*', '.'},
                        {'.', '.', '.', '*', '.', '.'},
                        {'.', '.', '.', '.', '.', '.'},
                };

        Game game = new Game(initialGrid);

        char[][] generation = game.Tick();

        assertArrayEquals(generation, initialGrid);

    }

    @Test
    public void Test_Blinker()
    {
        char[][] initialGrid = new char[][]
        {
            {'.', '.', '.'},
            {'*', '*', '*'},
            {'.', '.', '.'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '*', '.'},
            {'.', '*', '.'},
            {'.', '*', '.'},
        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '.', '.'},
            {'*', '*', '*'},
            {'.', '.', '.'},
        };

        Game game = new Game(initialGrid);

        char[][] actualGeneration = game.Tick();

        assertArrayEquals(actualGeneration, expectedGeneration1);

        actualGeneration = game.Tick();

        assertArrayEquals(actualGeneration, expectedGeneration2);

    }

    @Test
    public void Test_Glider()
    {
        char[][] initialGrid = new char[][]
        {
            {'.', '*', '.'},
            {'.', '.', '*'},
            {'*', '*', '*'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '.', '.'},
            {'.', '*', '.'},
            {'.', '*', '*'},
            {'.', '*', '.'},
        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '.', '.'},
            {'.', '.', '*'},
            {'*', '.', '*'},
            {'.', '*', '*'},
        };

        Game game = new Game(initialGrid);

        char[][] actualGeneration = game.Tick();

        assertArrayEquals(actualGeneration, expectedGeneration1);

        actualGeneration = game.Tick();

        assertArrayEquals(actualGeneration, expectedGeneration2);


    }
}
