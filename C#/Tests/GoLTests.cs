﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class GoLTests
    {
        [Test]
        public void Test_Barge()
        {
            var initialGrid = new char[,]
            {
               {'.', '.', '.', '.', '.', '.'}, 
               {'.', '.', '*', '.', '.', '.'}, 
               {'.', '*', '.', '*', '.', '.'}, 
               {'.', '.', '*', '.', '*', '.'}, 
               {'.', '.', '.', '*', '.', '.'}, 
               {'.', '.', '.', '.', '.', '.'}, 
            };

            var game = new Game(initialGrid);

            char[,] generation = game.Tick();

            Assert.That(generation, Is.EqualTo(initialGrid));

        }

        [Test]
        public void Test_Blinker()
        {
            var initialGrid = new char[,]
            {
               {'.', '.', '.'}, 
               {'*', '*', '*'}, 
               {'.', '.', '.'}, 
            };

            var expectedGeneration1 = new char[,]
            {
               {'.', '*', '.'}, 
               {'.', '*', '.'}, 
               {'.', '*', '.'}, 
            };

            var expectedGeneration2 = new char[,]
            {
               {'.', '.', '.'}, 
               {'*', '*', '*'}, 
               {'.', '.', '.'}, 
            };

            var game = new Game(initialGrid);

            char[,] actualGeneration = game.Tick();

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            actualGeneration = game.Tick();

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));

        }

        [Test]
        public void Test_Glider()
        {
            var initialGrid = new char[,]
            {
               {'.', '*', '.'}, 
               {'.', '.', '*'}, 
               {'*', '*', '*'}, 
            };

            var expectedGeneration1 = new char[,]
            {
               {'.', '.', '.'}, 
               {'.', '*', '.'}, 
               {'.', '*', '*'}, 
               {'.', '*', '.'}, 
            };

            var expectedGeneration2 = new char[,]
            {
               {'.', '.', '.'}, 
               {'.', '.', '*'}, 
               {'*', '.', '*'}, 
               {'.', '*', '*'}, 
            };

            var game = new Game(initialGrid);

            char[,] actualGeneration = game.Tick();

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            actualGeneration = game.Tick();

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));


        }
    }

    public class Game
    {
        public Game(char[,] initialGrid)
        {
            
        }

        public char[,] Tick()
        {
            return null;
        }
    }
}
